package com.cs5.pojo;

import java.util.ArrayList;
import java.util.Date;

public class OrderPojo {
	
	private int orderId;
	private int voucherId;
	private String coffeeType;
	private String coffeeSize;
	private String customerName;
	private long customerPhone;
	private Date date;
	private float result;
	private ArrayList<String> coffeeAddOn;
	
	public OrderPojo() {
		this.coffeeAddOn=new ArrayList<String>();
	}
	
	public OrderPojo(int voucherId, String coffeeType, String coffeeSize, String customerName, long customerPhone,
			Date date, float result, ArrayList<String> coffeeAddOn) {
		super();
		this.voucherId = voucherId;
		this.coffeeType = coffeeType;
		this.coffeeSize = coffeeSize;
		this.customerName = customerName;
		this.customerPhone = customerPhone;
		this.date = date;
		this.result = result;
		this.coffeeAddOn = coffeeAddOn;
	}
	public  int getOrderId() {
		return orderId;
	}
	public  void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public int getVoucherId() {
		return voucherId;
	}
	public void setVoucherId(int voucherId) {
		this.voucherId = voucherId;
	}
	public String getCoffeeType() {
		return coffeeType;
	}
	public void setCoffeeType(String coffeeType) {
		this.coffeeType = coffeeType;
	}
	public String getCoffeeSize() {
		return coffeeSize;
	}
	public void setCoffeeSize(String coffeeSize) {
		this.coffeeSize = coffeeSize;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public long getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(long customerPhone) {
		this.customerPhone = customerPhone;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public float getResult() {
		return result;
	}
	public void setResult(float result) {
		this.result = result;
	}
	public ArrayList<String> getCoffeeAddOn() {
		return coffeeAddOn;
	}
	public void setCoffeeAddOn(ArrayList<String> coffeeAddOn) {
		this.coffeeAddOn = coffeeAddOn;
	}

	@Override
	public String toString() {
		return "OrderPojo [orderId=" + orderId + ", voucherId=" + voucherId + ", coffeeType=" + coffeeType
				+ ", coffeeSize=" + coffeeSize + ", customerName=" + customerName + ", customerPhone=" + customerPhone
				+ ", date=" + date + ", result=" + result + ", coffeeAddOn=" + coffeeAddOn + "]";
	}
	
}
