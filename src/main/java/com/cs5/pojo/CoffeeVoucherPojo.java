package com.cs5.pojo;

import java.util.Date;

public class CoffeeVoucherPojo {
	
	private String coffeeVoucherName;
	private int coffeeVoucherId;
	private Date coffeeVoucherIssue;
	private Date coffeeVoucherEnd;
	public CoffeeVoucherPojo(String coffeeVoucherName, int coffeeVoucherId, Date coffeeVoucherIssue,
			Date coffeeVoucherEnd) {
		super();
		this.coffeeVoucherName = coffeeVoucherName;
		this.coffeeVoucherId = coffeeVoucherId;
		this.coffeeVoucherIssue = coffeeVoucherIssue;
		this.coffeeVoucherEnd = coffeeVoucherEnd;
	}
	public String getCoffeeVoucherName() {
		return coffeeVoucherName;
	}
	public void setCoffeeVoucherName(String coffeeVoucherName) {
		this.coffeeVoucherName = coffeeVoucherName;
	}
	public int getCoffeeVoucherId() {
		return coffeeVoucherId;
	}
	public void setCoffeeVoucherId(int coffeeVoucherId) {
		this.coffeeVoucherId = coffeeVoucherId;
	}
	public Date getCoffeeVoucherIssue() {
		return coffeeVoucherIssue;
	}
	public void setCoffeeVoucherIssue(Date coffeeVoucherIssue) {
		this.coffeeVoucherIssue = coffeeVoucherIssue;
	}
	public Date getCoffeeVoucherEnd() {
		return coffeeVoucherEnd;
	}
	public void setCoffeeVoucherEnd(Date coffeeVoucherEnd) {
		this.coffeeVoucherEnd = coffeeVoucherEnd;
	}
	
	

}
