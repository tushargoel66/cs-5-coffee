package com.cs5.pojo;

public class SalesBoyPojo {
	private String salesBoyName;
	private int salesBoyId;
	private long salesBoyPhone;
	private String userName, password;
	
	public SalesBoyPojo(String salesBoyName, int salesBoyId, long salesBoyPhone, String userName, String password) {
		super();
		this.salesBoyName = salesBoyName;
		this.salesBoyId = salesBoyId;
		this.salesBoyPhone = salesBoyPhone;
		this.userName = userName;
		this.password = password;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSalesBoyName() {
		return salesBoyName;
	}
	public void setSalesBoyName(String salesBoyName) {
		this.salesBoyName = salesBoyName;
	}
	public int getSalesBoyId() {
		return salesBoyId;
	}
	public void setSalesBoyId(int salesBoyId) {
		this.salesBoyId = salesBoyId;
	}
	public long getSalesBoyPhone() {
		return salesBoyPhone;
	}
	public void setSalesBoyPhone(long salesBoyPhone) {
		this.salesBoyPhone = salesBoyPhone;
	}
	

}
