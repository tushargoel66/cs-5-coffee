package com.cs5.pojo;

public class CoffeeSizePojo {

	private String coffeeSize;
	private int coffeeSizeId;
	private int coffeeSizeMultiFactor;
	
	
	public CoffeeSizePojo(String coffeeSize, int coffeeSizeId, int coffeeSizeMultiFactor) {
		super();
		this.coffeeSize = coffeeSize;
		this.coffeeSizeId = coffeeSizeId;
		this.coffeeSizeMultiFactor = coffeeSizeMultiFactor;
	}
	public String getCoffeeSize() {
		return coffeeSize;
	}
	public void setCoffeeSize(String coffeeSize) {
		this.coffeeSize = coffeeSize;
	}
	public int getCoffeeSizeId() {
		return coffeeSizeId;
	}
	public void setCoffeeSizeId(int coffeeSizeId) {
		this.coffeeSizeId = coffeeSizeId;
	}
	public int getCoffeeSizeMultiFactor() {
		return coffeeSizeMultiFactor;
	}
	public void setCoffeeSizeMultiFactor(int coffeeSizeMultiFactor) {
		this.coffeeSizeMultiFactor = coffeeSizeMultiFactor;
	}
	@Override
	public String toString() {
		return "CoffeeSizePojo [coffeeSize=" + coffeeSize + ", coffeeSizeId=" + coffeeSizeId
				+ ", coffeeSizeMultiFactor=" + coffeeSizeMultiFactor + "]";
	}
	
}
