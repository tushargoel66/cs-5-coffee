package com.cs5.pojo;

public class CustomerPojo {

	private String customerName;
	private long customerPhone;
	private boolean preferredCustomer;
	private int numberOfOrder;
	
	
	public CustomerPojo(String customerName, long customerPhone, boolean preferredCustomer) {
		super();
		this.customerName = customerName;
		this.customerPhone = customerPhone;
		this.preferredCustomer = preferredCustomer;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public long getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(long customerPhone) {
		this.customerPhone = customerPhone;
	}
	public boolean isPreferredCustomer() {
		return preferredCustomer;
	}
	public void setPreferredCustomer(boolean preferredCustomer) {
		this.preferredCustomer = preferredCustomer;
	}
	public int getNumberOfOrder() {
		return numberOfOrder;
	}
	public void setNumberOfOrder(int numberOfOrder) {
		this.numberOfOrder = numberOfOrder;
	}
}
