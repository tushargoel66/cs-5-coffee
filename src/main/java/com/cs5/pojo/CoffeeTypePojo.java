package com.cs5.pojo;

public class CoffeeTypePojo {
	private String coffeeName;
	private int coffeeId;
	private double coffeePrice;
	
	
	public CoffeeTypePojo(String coffeeName, int coffeeId, double coffeePrice) {
		super();
		this.coffeeName = coffeeName;
		this.coffeeId = coffeeId;
		this.coffeePrice = coffeePrice;
	}
	public String getCoffeeName() {
		return coffeeName;
	}
	public void setCoffeeName(String coffeeName) {
		this.coffeeName = coffeeName;
	}
	public int getCoffeeId() {
		return coffeeId;
	}
	public void setCoffeeId(int coffeeId) {
		this.coffeeId = coffeeId;
	}
	public double getCoffeePrice() {
		return coffeePrice;
	}
	public void setCoffeePrice(double coffeePrice) {
		this.coffeePrice = coffeePrice;
	}
}
