package com.cs5.pojo;

public class CoffeeAddOnPojo {

	private String coffeeAddOn;
	private int coffeeAddOnId;
	private double coffeeAddOnPrice;
	
	
	public CoffeeAddOnPojo(String coffeeAddOn, int coffeeAddOnId, double coffeeAddOnPrice) {
		super();
		this.coffeeAddOn = coffeeAddOn;
		this.coffeeAddOnId = coffeeAddOnId;
		this.coffeeAddOnPrice = coffeeAddOnPrice;
	}
	public String getCoffeeAddOn() {
		return coffeeAddOn;
	}
	public void setCoffeeAddOn(String coffeeAddOn) {
		this.coffeeAddOn = coffeeAddOn;
	}
	public int getCoffeeAddOnId() {
		return coffeeAddOnId;
	}
	public void setCoffeeAddOnId(int coffeeAddOnId) {
		this.coffeeAddOnId = coffeeAddOnId;
	}
	public double getCoffeeAddOnPrice() {
		return coffeeAddOnPrice;
	}
	public void setCoffeeAddOnPrice(double coffeeAddOnPrice) {
		this.coffeeAddOnPrice = coffeeAddOnPrice;
	}
}
