package com.cs5.repo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.cs5.pojo.CoffeeAddOnPojo;
import com.cs5.pojo.CoffeeSizePojo;
import com.cs5.pojo.CoffeeTypePojo;
import com.cs5.pojo.OrderPojo;

public interface MenuGenerationRepo {
	public List<CoffeeTypePojo> coffeeMenu() throws ClassNotFoundException, SQLException;
	public boolean login(String username) throws SQLException, ClassNotFoundException;
	public List<CoffeeSizePojo> coffeeTypeMenu() throws ClassNotFoundException, SQLException;
	public List<CoffeeAddOnPojo> coffeeAddOnMenu() throws ClassNotFoundException, SQLException;
	public ResultSet getCustomer(long phone) throws ClassNotFoundException, SQLException;
	public void placeOrder(OrderPojo order) throws ClassNotFoundException, SQLException ;
	public ResultSet getOrderDetails(int orderId) throws ClassNotFoundException, SQLException ;
	public void intsetCustomer(String name, long phone) throws ClassNotFoundException, SQLException;
}
