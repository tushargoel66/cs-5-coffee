package com.cs5.repoimp;



import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.cs5.pojo.CoffeeAddOnPojo;
import com.cs5.pojo.CoffeeSizePojo;
import com.cs5.pojo.CoffeeTypePojo;
import com.cs5.pojo.OrderPojo;
import com.cs5.repo.MenuGenerationRepo;
import com.cs5.services.DBConnection;

public class MenuGenerationRepoImpl implements MenuGenerationRepo{
	
	String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CoffeeTypePojo> coffeeMenu() throws ClassNotFoundException, SQLException {
		DBConnection database=new DBConnection();
		String query="select * from coffee";
		PreparedStatement preparedStatements=database.getConnection().prepareStatement(query);
		
		List<CoffeeTypePojo> coffeeType=new ArrayList<CoffeeTypePojo>();
		
		ResultSet rs=preparedStatements.executeQuery();
		while(rs.next()) {
			CoffeeTypePojo type=new CoffeeTypePojo(rs.getString(1), rs.getInt(2), rs.getFloat(3));
			coffeeType.add(type);			
		} 
		return coffeeType;
	}

	public boolean login(String username) throws SQLException, ClassNotFoundException {
		DBConnection database=new DBConnection();
		String query="select BoyId,BoyName from salesboy where BoyId=?";
		PreparedStatement preparedStatements=database.getConnection().prepareStatement(query);
		preparedStatements.setString(1, username);
		
		ResultSet rs=preparedStatements.executeQuery();
		if(rs.next()) {
			name=rs.getString(2);
			return true;
		} 
		return false;
	}

	public List<CoffeeSizePojo> coffeeTypeMenu() throws ClassNotFoundException, SQLException {
		DBConnection database=new DBConnection();
		String query="select * from coffeesize";
		PreparedStatement preparedStatements=database.getConnection().prepareStatement(query);
		
		List<CoffeeSizePojo> coffeeSizeType=new ArrayList<CoffeeSizePojo>();
		
		ResultSet rs=preparedStatements.executeQuery();
		while(rs.next()) {
			CoffeeSizePojo type=new CoffeeSizePojo(rs.getString(1), rs.getInt(2), rs.getInt(3));
			coffeeSizeType.add(type);			
		} 
		return coffeeSizeType;
	}

	public List<CoffeeAddOnPojo> coffeeAddOnMenu() throws ClassNotFoundException, SQLException {
		DBConnection database=new DBConnection();
		String query="select * from cofeeaddon";
		PreparedStatement preparedStatements=database.getConnection().prepareStatement(query);
		
		List<CoffeeAddOnPojo> coffeeaddonType=new ArrayList<CoffeeAddOnPojo>();
		
		ResultSet rs=preparedStatements.executeQuery();
		while(rs.next()) {
			CoffeeAddOnPojo addon=new CoffeeAddOnPojo(rs.getString(1), rs.getInt(2), rs.getFloat(3));
			coffeeaddonType.add(addon);			
		} 
		return coffeeaddonType;
	}
	
	public ResultSet getCustomer(long phone)throws ClassNotFoundException, SQLException {
		DBConnection database=new DBConnection();
		String query="select * from customer where CustomerPhone=?";
		PreparedStatement preparedStatements=database.getConnection().prepareStatement(query);
		preparedStatements.setLong(1, phone);
		return preparedStatements.executeQuery();
	}

	public void intsetCustomer(String name, long phone) throws ClassNotFoundException, SQLException {
		DBConnection database=new DBConnection();
		String query="insert into customer values(?,?,?,?)";
		PreparedStatement preparedStatements=database.getConnection().prepareStatement(query);
		preparedStatements.setLong(4, phone);
		preparedStatements.setString(1, name);
		preparedStatements.setInt(2, 0);
		preparedStatements.setInt(3, 0);
		preparedStatements.executeUpdate();
	}

	public void placeOrder(OrderPojo order) throws ClassNotFoundException, SQLException {
		DBConnection database=new DBConnection();
		
		for(String i:order.getCoffeeAddOn()) {
			String query="insert into orders values(?,?,?,?,?,?,?,?,?)";
			PreparedStatement preparedStatements=database.getConnection().prepareStatement(query);
			preparedStatements.setString(1, order.getCoffeeType());
			preparedStatements.setString(2, order.getCoffeeType());
			preparedStatements.setString(3, order.getCustomerName());
			preparedStatements.setString(4, Long.toString(order.getCustomerPhone()));
			preparedStatements.setString(5, order.getDate().toString());
			preparedStatements.setFloat(6, order.getResult());			
			preparedStatements.setString(7, i);
			preparedStatements.setString(8, "");
			preparedStatements.setInt(9, order.getOrderId());
			preparedStatements.executeUpdate();
		}
	}

	public ResultSet getOrderDetails(int orderId) throws ClassNotFoundException, SQLException {
		DBConnection database=new DBConnection();
		String query="select * from orders where orderid=?";
		PreparedStatement preparedStatements=database.getConnection().prepareStatement(query);
		preparedStatements.setLong(1, orderId);
		return preparedStatements.executeQuery();
	}

}
