package com.cs5.driver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cs5.pojo.CoffeeAddOnPojo;
import com.cs5.pojo.CoffeeSizePojo;
import com.cs5.pojo.CoffeeTypePojo;
import com.cs5.pojo.OrderPojo;
import com.cs5.repoimp.MenuGenerationRepoImpl;

public class CS5Main {
	
	static int orderId=1;
	
	public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException, InterruptedException {
		
		String name="";
		
		BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
		MenuGenerationRepoImpl salesBoy=new MenuGenerationRepoImpl();
		//Sales Boy Login
		
		System.out.println("Welcome\nEnter your Credentials");
		while(true) {
			
			System.out.println("ID: ");
			String username=bufferedReader.readLine();
			if(salesBoy.login(username)) {
				name=salesBoy.getName();
				System.out.println("Successfully Logged IN!!!\nWelcome "+name);
				break;
			} 
			System.out.println("Enter your correct credentials please!!!");
			System.out.println("----------------------------------------------");
		}
		System.out.println("============================================");
		
		List<CoffeeTypePojo> coffeeMenu=salesBoy.coffeeMenu();
		List<CoffeeSizePojo> coffeeSizePojos=salesBoy.coffeeTypeMenu();
		List<CoffeeAddOnPojo> addOnList=salesBoy.coffeeAddOnMenu();
		
		//Operation performed by sales boy
		
		while(true) {
			System.out.println("Operation:\n1.Add Order\n2.Display Orders\n3.Show Coffee Menu\n4.Exit");
			int choice=Integer.parseInt(bufferedReader.readLine());
			switch(choice) {
				case 1:
					OrderPojo order=new OrderPojo();
					List<Double> price=new ArrayList<Double>();
					while(true) {
						System.out.println("--------------------------------");
						for(int i=0;i<coffeeMenu.size();i++) {
							System.out.println(coffeeMenu.get(i).getCoffeeId()+". "+coffeeMenu.get(i).getCoffeeName()+"    "+coffeeMenu.get(i).getCoffeePrice());
						}
						boolean flag=false;
						System.out.println("Choose a type of coffee: ");
						int type=Integer.parseInt(bufferedReader.readLine());
						for(int i=0;i<coffeeMenu.size();i++) {
							if(coffeeMenu.get(i).getCoffeeId()==type) {
								order.setCoffeeType(coffeeMenu.get(i).getCoffeeName());
								price.add(coffeeMenu.get(i).getCoffeePrice());
								flag=true;
							}
						}
						if(flag)
							break;
						System.out.println("Please select a correct code");
					}
					
					while(true) {
						System.out.println("--------------------------------");
						for(int i=0;i<coffeeSizePojos.size();i++) {
							System.out.println(coffeeSizePojos.get(i).getCoffeeSizeId()+". "+coffeeSizePojos.get(i).getCoffeeSize()+"    "+coffeeSizePojos.get(i).getCoffeeSizeMultiFactor());
						}
						boolean flag=true;
						System.out.println("Choose the coffee size: ");
						int size=Integer.parseInt(bufferedReader.readLine());
						for(int i=0;i<coffeeSizePojos.size();i++) {
							if(coffeeSizePojos.get(i).getCoffeeSizeId()==size) {
								order.setCoffeeSize(coffeeSizePojos.get(i).getCoffeeSize());
								price.add(coffeeSizePojos.get(i).getCoffeeSizeMultiFactor()*price.get(0));
								flag=false;
							}
						}
						if(!flag)
							break;
						System.out.println("Please select a correct code");
					}

					while(true) {
//						while(true) {
						System.out.println("--------------------------------");
						for(int i=0;i<addOnList.size();i++) {
							System.out.println(addOnList.get(i).getCoffeeAddOnId()+". "+addOnList.get(i).getCoffeeAddOn()+"    "+addOnList.get(i).getCoffeeAddOnPrice());
						}
//							boolean flag=true;
						System.out.println("Choose the coffee size: ");
						int addons=Integer.parseInt(bufferedReader.readLine());
						for(int i=0;i<addOnList.size();i++) {
							if(addOnList.get(i).getCoffeeAddOnId()==addons) {
								order.getCoffeeAddOn().add(addOnList.get(i).getCoffeeAddOn());
								price.add(addOnList.get(i).getCoffeeAddOnPrice());
//								flag=false;
							}
						}
//							if(!flag)
//								break;
//						}
						System.out.println("Wanna add more???(Y/N)");
						String c=bufferedReader.readLine();
						if(c.equalsIgnoreCase("N"))
							break;
					}
					
					System.out.println("Enter customer details: \nEnter phone number: ");
					long phone=Long.parseLong(bufferedReader.readLine());
					ResultSet result=salesBoy.getCustomer(phone);
					if(result.next()) {
						order.setCustomerName(result.getString(1));
						order.setDate(new Date());
						order.setCustomerPhone(phone);
						order.setOrderId(orderId++);
					}else {
						System.out.println("Enter more details: ");
						System.out.println("Enter name: ");
						String cname=bufferedReader.readLine();
						salesBoy.intsetCustomer(cname, phone);
						order.setCustomerName(cname);
						order.setDate(new Date());
						order.setCustomerPhone(phone);
						order.setOrderId(orderId++);
					}
					double p=0;
					for(double i:price) {
						p+=i;
					}					
					order.setResult((float)p);
					System.out.println("============================================");
					System.out.println("Please verify your order!!!");
					System.out.println(order.getCoffeeType()+": "+price.get(0));
					System.out.println(order.getCoffeeSize()+": "+price.get(1));
					int id=2;
					for(String i:order.getCoffeeAddOn())
						System.out.println(i+": "+price.get(id++));
					System.out.println("Date: "+order.getDate());
					System.out.println("Your total bill is : "+order.getResult()+"/-");
					System.out.println("============================================");
					System.out.println("Wanna continue???(y/n)");
					if(bufferedReader.readLine().equals("y"))
						salesBoy.placeOrder(order);
					break;
				case 2:
					System.out.println("Please enter order ID: ");
					int orderId=Integer.parseInt(bufferedReader.readLine());
					ResultSet resultt=salesBoy.getOrderDetails(orderId);
					boolean flag=false;
					while(resultt.next()) {
						System.out.println("Coffee Type: "+resultt.getString(1));
						System.out.println("Coffee Size: "+resultt.getString(2));
						System.out.println("Customer Name: "+resultt.getString(3));
						System.out.println("Customer Phone: "+resultt.getString(4));
						System.out.println("Date: "+resultt.getString(5));
						System.out.println("Bill: "+resultt.getFloat(6));
						System.out.println("OrderId: "+resultt.getInt(9));
						flag=true;
					}
					if(!flag)
						System.out.println("No such order found!!!!");
					break;
				case 3:
					System.out.println("============================================");
					for(int i=0;i<coffeeMenu.size();i++) {
						System.out.println(coffeeMenu.get(i).getCoffeeId()+". "+coffeeMenu.get(i).getCoffeeName()+"    "+coffeeMenu.get(i).getCoffeePrice());
					}
					System.out.println("============================================");
					break;
				case 4:
					System.exit(0);
				default:
					System.out.println("Enter a valid choice!!!!");
			}
		}
		
	}
}

